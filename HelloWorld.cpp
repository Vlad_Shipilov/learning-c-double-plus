﻿#include <iostream>
using namespace std;

class Animal
{
public:
    virtual void Voice() const = 0
    {
        cout << "Voice\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() const override
    {
        cout << "Meow\n";
    }
};

class Dog : public Animal
{
public:
    void Voice() const override
    {
        cout << "Woof\n";
    }
};

class Pig : public Animal
{
public:
    void Voice() const override
    {
        cout << "Oink\n";
    }
};

class Horse : public Animal
{
public:
    void Voice() const override
    {
        cout << "Neigh\n";
    }
};

int main()
{
    Animal* animals[4];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Pig();
    animals[3] = new Horse();

    for (Animal* a : animals)
        a->Voice();
}