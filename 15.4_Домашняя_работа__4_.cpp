/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>

void print(bool c, int n) 
{
    for (int i = 0; i <= n; i++)
        {
            if (i % 2 == c)
                std::cout << i << " ";
        }

    
}

int main()
{
    bool c; 
    int x;
    std::cout << "Введите число:";
    std::cin >> x;
    std::cout << "Введите '1' для отображения нечетных чисел, либо - '0' для отображения четных:";
    std::cin >> c;
    
    print(c, x);
    
    return 0;
}
